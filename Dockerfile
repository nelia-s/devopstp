FROM nginx:latest

WORKDIR /usr/share/nginx/html

#RUN rm -rf index.html
RUN apt update && apt install git -y

RUN git clone https://gitlab.com/nelia-s/devopstp.git
RUN mv /usr/share/nginx/html/devopstp/index.html /usr/share/nginx/html && rm -rf /usr/share/nginx/html/devopstp

ARG IMG
ARG TitreHeader
ARG Footer
RUN sed -i 's/{{Titre}}/'"${TitreHeader}"'/g' /usr/share/nginx/html/index.html &&\
    sed -i 's/{{PeDePag}}/'"${Footer}"'/g' /usr/share/nginx/html/index.html
RUN sed -i 's/{{IMAGE_URL}}/${IMG}/g' /usr/share/nginx/html/index.html